package main // import "gitlab.com/mavogel/release-testing"

import (
	"flag"
	"fmt"
	"os"

	"github.com/xanzy/go-gitlab"
)

func main() {
	fmt.Println("bla")
	var run bool
	flag.BoolVar(&run, "r", false, "Just shows the binary runs. Used for homebrew testing")
	flag.Parse()
	if run {
		fmt.Println("Yeah it runs! 1elf")
		return
	}

	token, ok := os.LookupEnv("GITLAB_TOKEN")
	if !ok {
		panic("fail")
	}
	client := gitlab.NewClient(nil, token)

	gitlabBaseURL := "https://gitlab.com"
	// releaseProjectPID := "mavogel/release-testing" // 12642869
	// file := "test_upload3.txt"

	// DONE :)))
	// testingReleases(client, gitlabBaseURL, releaseProjectPID, file)
	creatingFiles(client, gitlabBaseURL)
}

func testingReleases(client *gitlab.Client, gitlabBaseURL, releaseProjectPID, file string) {
	// // opts := gitlab.OptionFunc
	// TODO check if file exists -> not possible cuz gilab always generates a new hash although the file has not changed..
	// they probably include a timestamp... no idea
	projectFile, _, err := client.Projects.UploadFile(releaseProjectPID, file, nil)
	if err != nil {
		panic(err)
	}
	fmt.Printf("file: %v\n", projectFile.URL)

	name := "v0.0.3"
	ref := "fe1b3be8fa83a486decc5b81a054dd9ab3d289fe" // Devskim ignore: DS173237
	tagName := name

	_, resp, err := client.Releases.GetRelease(releaseProjectPID, tagName)
	fmt.Printf("status: %v\n", resp.StatusCode)
	if err != nil && resp.StatusCode == 403 {
		desc := "Desc"
		release, _, err := client.Releases.CreateRelease(releaseProjectPID, &gitlab.CreateReleaseOptions{
			Name:        &name,
			Description: &desc,
			Ref:         &ref,
			TagName:     &tagName,
		})

		if err != nil {
			panic(err)
		}

		fmt.Printf("created release: %v\n", release.Name)

		linkURL := gitlabBaseURL + "/" + releaseProjectPID + projectFile.URL
		releaseLink, _, err := client.ReleaseLinks.CreateReleaseLink(releaseProjectPID, tagName, &gitlab.CreateReleaseLinkOptions{
			Name: &file,
			URL:  &linkURL,
		})
		if err != nil {
			panic(err)
		}
		fmt.Printf("release link: %v ->  %v\n", releaseLink.ID, releaseLink.URL)
	} else {
		newDesc := "Desc updated!<br>Next line<br>[1338285](https://gitlab.com/mavogel/release-testing/commit/1338285cf4ebc5e590620a3213fc1c845f0c8836)<br>[fe1b3be](https://gitlab.com/mavogel/release-testing/commit/fe1b3be8fa83a486decc5b81a054dd9ab3d289fe)"
		release, _, err := client.Releases.UpdateRelease(releaseProjectPID, tagName, &gitlab.UpdateReleaseOptions{
			Name:        &name,
			Description: &newDesc,
		})

		if err != nil {
			panic(err)
		}

		fmt.Printf("updated release: %v\n", release.Name)

		linkURL := gitlabBaseURL + "/" + releaseProjectPID + projectFile.URL // from the re-upload
		link := 4649                                                         // from create link
		releaseLink, _, err := client.ReleaseLinks.UpdateReleaseLink(releaseProjectPID, tagName, link, &gitlab.UpdateReleaseLinkOptions{
			Name: &file,
			URL:  &linkURL,
		})
		if err != nil {
			panic(err)
		}

		fmt.Printf("release link updated: %v ->  %v\n", releaseLink.ID, releaseLink.URL)
	}
}

func creatingFiles(client *gitlab.Client, gitlabBaseURL string) {
	brewTapProjectPID := "mavogel/homebrew-tab"
	existingFile := "Formula/release-testing-program.rb"
	// nonExistingFile := "i-do-not-exist"
	fileName := existingFile

	ref := "master" // or commit ref, maybe :) Do it!! With a ptr now
	opts := &gitlab.GetFileOptions{
		Ref: &ref,
	}

	file, res, err := client.RepositoryFiles.GetFile(brewTapProjectPID, fileName, opts)
	fmt.Printf("res.StatusCode: %v\n", res.StatusCode)
	if err != nil && res.StatusCode != 404 {
		fmt.Printf("res: %v", res)
		panic(err)
	}

	if res.StatusCode == 404 {
		fmt.Println("file not found")
	} else {
		fmt.Printf("res: %#v\n", res)
		fmt.Printf("file: %#v\n", file)
	}
}
