# release testing
... a repo for testing release to gitlab =) :tada:

## building it
for artifacts @ `goreleaser` testing =)

```sh
export GITLAB_TOKEN="xyz"
# OR
export GITHUB_TOKEN="xyz"
# OR
export GITEA_TOKEN="xyz"


# run to test lib locally => don't do this. Just for quick runs
go run main.go

# or with local releaser build assuming both project are on the same dir level
## run this in the goreleaser repo
go build -o goreleaser-gitlab && mv -f goreleaser-gitlab ../release-testing

## run in this repo
git tag 0.2.1

# adapt the origin remote to github or gitlab, e.g.
git remote rename origin pub-github
git remote rename pub-gitlab origin

git push origin master 0.2.1
./goreleaser-gitlab  --config=./goreleaser-wo-docker.yml --debug --rm-dist

# checck brew as well
brew tap mavogel/tools git@gitlab.com:mavogel/homebrew-tab.git
brew install mavogel/tools/release-testing

# ci and docker testing
## binary needs to be in the root
GOOS=linux GOARCH=amd64 go build -v -i
docker build -t mavogel/release-testing:latest .
```

## testing against older gitlab versions
- based on https://gitlab.com/gitlab-org/gitlab-ce/issues/50851
- adapt the version in `docker-compose-gitlab.yaml` from [hub.docker](https://hub.docker.com/r/gitlab/gitlab-ce/tags)
- adapt your `etc/hosts` to `127.0.0.1       localhost gitlab-web`
```sh
docker-compose -f docker-compose-gitlab.yml up -d
docker-compose -f docker-compose-gitlab.yml logs -tf
```
- access then via `localhost:8080`
  - user: `root`
  - password: <the-one-you-set> -> `root123!`
- create a user via the admin area cuz registering won't work cuz no emails are sent
- add an ssh-key
- add remote: `git remote add local-gitlab ssh://git@gitlab-web:2222/mavogel/release-testing.git`
- `git remote rename local-gitlab origin` because goreleaser only tackles the remote `origin` atm

## testing against gitea
- https://docs.gitea.io/en-us/install-with-docker/
- - adapt your `etc/hosts` to `127.0.0.1       localhost gitea-web`
```sh
docker-compose -f docker-compose-gitea.yml up -d
docker-compose -f docker-compose-gitea.yml logs -tf
```
- access then via `http://localhost:3000/`
  - user: `mavogel`
  - password: <the-one-you-set>
- create a user cuz first created user becomes admin: https://github.com/go-gitea/gitea/issues/4120#issuecomment-394486741
- gitea adapts the exposed port and hostname correctly but for gitea adapt it to: `git remote add local-gitea ssh://git@gitea-web:222/mavogel/release-testing.git`
- `git remote rename local-gitea origin` because goreleaser only tackles the remote `origin` atm
- add your pubkey to gitea: `cat ~/.ssh/id_rsa.pub|pbcopy` -> `http://localhost:3000/user/settings/keys`
- push master `git push origin master`
- create a `homebrew-tap` repository in the local gitea to test homebrews **WITH** an empty `Formula` folder
- create a user token `http://localhost:3000/user/settings/applications` + export it
- get the latest binary: `mv -f ../../github/goreleaser/goreleaser goreleaser-gitea`
- run the release `./goreleaser-gitea --config=./goreleaser-gitea-local.yml --debug --rm-dist`

```
brew tap mavogel/gitea-tools ssh://git@localhost:222/mavogel/homebrew-tap.git
brew install mavogel/gitea-tools/release-testing
```